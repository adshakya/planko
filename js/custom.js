$(window).on("load", function () {

    "use strict";

    /* ===================================
            Loading Timeout
     ====================================== */

    $('.side-menu.hidden').removeClass('hidden');

    setTimeout(function () {
        $(".loader").fadeOut("fast");
    }, 1);


});

jQuery(function ($) {


    "use strict";


    /* ===================================
         Scroll
    ====================================== */


    $(window).on('scroll', function () {
        if ($(this).scrollTop() > 120) { // Set position from top to add class
            $('header').addClass('header-appear');
        }
        else {
            $('header').removeClass('header-appear');
        }
    });

    //scroll to appear
    $(window).on('scroll', function () {
        if ($(this).scrollTop() > 500)
            $('.scroll-top-arrow').fadeIn('slow');
        else
            $('.scroll-top-arrow').fadeOut('slow');
    });

    //Click event to scroll to top
    $(document).on('click', '.scroll-top-arrow', function () {
        $('html, body').animate({scrollTop: 0}, 800);
        return false;
    });

    // $(".scroll").on("click", function (event) {
    //     event.preventDefault();
    //     $("html,body").animate({
    //         scrollTop: $(this.hash).offset().top - 60}, 1200);
    // });

    // scroll js

    $(".scroll[href^='#']").on('click', function(e) {
       e.preventDefault();
       $('html, body').animate({
           scrollTop: $(this.hash).offset().top -60
         }, 1000, function(){
              window.location.hash = this.hash;
         });

    });

 $('.center').slick({
      centerMode: true,
      centerPadding: '60px',
      slidesToShow: 3,
      arrows: false,
      autoplay: true,
      responsive: [
      {
      breakpoint: 1023,
      settings: {
      arrows: false,
      centerMode: true,
      centerPadding: '20px',
      slidesToShow: 2
      }
      },
      {
      breakpoint: 480,
      settings: {
      arrows: false,
      centerMode: true,
      centerPadding: '40px',
      slidesToShow: 1
      }
      }
      ]
      });

// blog js

 $('.blog-slider').slick({
      centerMode: true,
      centerPadding: '60px',
      slidesToShow: 4,
      arrows: false,
      autoplay: true,
      responsive: [
      {
      breakpoint: 768,
      settings: {
      arrows: false,
      centerMode: true,
      centerPadding: '40px',
      slidesToShow: 3
      }
      },
      {
      breakpoint: 480,
      settings: {
      arrows: false,
      centerMode: true,
      centerPadding: '40px',
      slidesToShow: 1
      }
      }
      ]
      });


// our client js

  $('.slider-main').slick({
      slidesToShow: 1,
      arrows: false,
      slidesToScroll: 1,
      asNavFor: '.slider-nav',
      vertical: true,
      autoplay: true,
      mobileFirst: true,
      adaptiveHeight: true,
      verticalSwiping: true
    });

    $('.slider-nav').slick({
      slidesToShow: 3,
      asNavFor: '.slider-main',
      vertical: true,
      slidesToScroll: 1,
       mobileFirst: true,
      focusOnSelect: true,
      autoplay: true,
      arrows: true
    });

    $(window).on('resize orientationchange', function() {
      if ($(window).width() > 1200) {
        $('.slider-nav').slick('unslick');
        $('.slider-nav').slick({
          slidesToShow: 3,
          asNavFor: '.slider-main',
          vertical: true,
           mobileFirst: true,
           slidesToScroll: 1,
          focusOnSelect: true,
          autoplay: true,
          centerMode: true,
          arrows: true
        });
      }
    });


});

$('.hover-content').hover(
     function(){ $(".hover-content.active").removeClass('active') },
     function(){ $(".hover-content:first-child").addClass('active') }
)



